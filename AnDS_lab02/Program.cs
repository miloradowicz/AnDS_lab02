﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;
using System.Reflection;
using MyOrganization;
using static MyOrganization.Organization.Query.Column;

namespace AnDS_lab02
{
  class Program
  {
    static void Generate(out Organization org, int cities_max, int branches_max, int departments_max, int teams_max, int employees_max)
    {
      StreamReader sr;
      string input;
      string[] cities;
      string[] branches;
      string[] male_first;
      string[] male_middle;
      string[] male_last;
      string[] female_first;
      string[] female_middle;
      string[] female_last;

      branches_max = branches_max < cities_max ? cities_max : branches_max;
      departments_max = departments_max < branches_max ? branches_max : departments_max;
      teams_max = teams_max < departments_max ? departments_max : teams_max;
      employees_max = employees_max < branches_max + departments_max + teams_max ? branches_max + departments_max + teams_max : employees_max;

      String[] separators = new String[] { "\r\n" };
      sr = new StreamReader(".\\cities.txt");
      input = sr.ReadToEnd();
      cities = input.Split(separators, StringSplitOptions.RemoveEmptyEntries);
      sr.Close();
      sr = new StreamReader(".\\branches.txt");
      input = sr.ReadToEnd();
      branches = input.Split(separators, StringSplitOptions.RemoveEmptyEntries);
      sr.Close();
      sr = new StreamReader(".\\male_first.txt");
      input = sr.ReadToEnd();
      male_first = input.Split(separators, StringSplitOptions.RemoveEmptyEntries);
      sr.Close();
      sr = new StreamReader(".\\male_middle.txt");
      input = sr.ReadToEnd();
      male_middle = input.Split(separators, StringSplitOptions.RemoveEmptyEntries);
      sr.Close();
      sr = new StreamReader(".\\male_last.txt");
      input = sr.ReadToEnd();
      male_last = input.Split(separators, StringSplitOptions.RemoveEmptyEntries);
      sr.Close();
      sr = new StreamReader(".\\female_first.txt");
      input = sr.ReadToEnd();
      female_first = input.Split(separators, StringSplitOptions.RemoveEmptyEntries);
      sr.Close();
      sr = new StreamReader(".\\female_middle.txt");
      input = sr.ReadToEnd();
      female_middle = input.Split(separators, StringSplitOptions.RemoveEmptyEntries);
      sr.Close();
      sr = new StreamReader(".\\female_last.txt");
      input = sr.ReadToEnd();
      female_last = input.Split(separators, StringSplitOptions.RemoveEmptyEntries);
      sr.Close();

      /* Generating */
      org = new Organization(cities_max, branches_max, departments_max, teams_max, employees_max);

      Organization.IQuery q;
      Random rnd = new Random((int)DateTime.Now.Ticks);
      DateTime e = new DateTime(2000, 12, 31);
      DateTime s = new DateTime(1945, 1, 1);
      Organization.Sex g;
      Console.WriteLine("Generating cities");
      for (int i = 0; i < cities_max;)
      {
        String n = cities[rnd.Next(cities.Length)];
        if (org.AddCity(n))
          i++;
      }
      q = org.GetCities();
      Console.WriteLine("Generating branches");
      for (int i = 0; i < branches_max;)
      {
        String n = branches[rnd.Next(branches.Length)];
        if (org.AddBranch($"Филиал \"{n}\"", q.ResultFields(i % cities_max)["ID"] as Organization.SID))
          i++;
      }
      q = org.GetBranches();
      Console.WriteLine("Generating branch directors");
      for (int i = 0; i < branches_max;)
      {
        String f, m, l;
        DateTime d = s.Add(new TimeSpan(rnd.Next((e - s).Days), 0, 0, 0));
        if (rnd.Next(2) == 0)
        {
          g = Organization.Sex.Female;
          f = female_first[rnd.Next(female_first.Length)];
          m = female_middle[rnd.Next(female_middle.Length)];
          l = female_last[rnd.Next(female_last.Length)];
        }
        else
        {
          g = Organization.Sex.Male;
          f = male_first[rnd.Next(male_first.Length)];
          m = male_middle[rnd.Next(male_middle.Length)];
          l = male_last[rnd.Next(male_last.Length)];
        }
        Organization.JobTitle t = Organization.JobTitle.Branch.BranchDirector;
        if (org.AddEmployee(l, f, m, d, g, q.ResultFields(i)["ID"] as Organization.SID, t))
          i++;
      }
      Console.WriteLine("Generating departments");
      for (int i = 0; i < departments_max; i++)
      {
        org.AddDepartment($"Отдел {i / branches_max + 1}", q.ResultFields(i % branches_max)["ID"] as Organization.SID);
      }
      q = org.GetDepartments();
      Console.WriteLine("Generating department heads");
      for (int i = 0; i < departments_max;)
      {
        String f, m, l;
        DateTime d = s.Add(new TimeSpan(rnd.Next((e - s).Days), 0, 0, 0));
        if (rnd.Next(2) == 0)
        {
          g = Organization.Sex.Female;
          f = female_first[rnd.Next(female_first.Length)];
          m = female_middle[rnd.Next(female_middle.Length)];
          l = female_last[rnd.Next(female_last.Length)];
        }
        else
        {
          g = Organization.Sex.Male;
          f = male_first[rnd.Next(male_first.Length)];
          m = male_middle[rnd.Next(male_middle.Length)];
          l = male_last[rnd.Next(male_last.Length)];
        }
        Organization.JobTitle t = Organization.JobTitle.Department.DepartmentHead;
        if (org.AddEmployee(l, f, m, d, g, q.ResultFields(i)["ID"] as Organization.SID, t))
          i++;
      }
      Console.WriteLine("Generating teams");
      for (int i = 0; i < teams_max; i++)
      {
        org.AddTeam($"Группа {i / departments_max + 1}", q.ResultFields(i % departments_max)["ID"] as Organization.SID);
      }
      q = org.GetTeams();
      Console.WriteLine("Generating team leads");
      for (int i = 0; i < teams_max;)
      {
        String f, m, l;
        DateTime d = s.Add(new TimeSpan(rnd.Next((e - s).Days), 0, 0, 0));
        if (rnd.Next(2) == 0)
        {
          g = Organization.Sex.Female;
          f = female_first[rnd.Next(female_first.Length)];
          m = female_middle[rnd.Next(female_middle.Length)];
          l = female_last[rnd.Next(female_last.Length)];
        }
        else
        {
          g = Organization.Sex.Male;
          f = male_first[rnd.Next(male_first.Length)];
          m = male_middle[rnd.Next(male_middle.Length)];
          l = male_last[rnd.Next(male_last.Length)];
        }
        Organization.JobTitle t = Organization.JobTitle.Team.TeamLead;
        if (org.AddEmployee(l, f, m, d, g, q.ResultFields(i)["ID"] as Organization.SID, t))
          i++;
      }
      Console.WriteLine("Generating employees");
      for (int i = branches_max + departments_max + teams_max; i < employees_max;)
      {
        String f, m, l;
        DateTime d = s.Add(new TimeSpan(rnd.Next((e - s).Days), 0, 0, 0));
        if (rnd.Next(2) == 0)
        {
          g = Organization.Sex.Female;
          f = female_first[rnd.Next(female_first.Length)];
          m = female_middle[rnd.Next(female_middle.Length)];
          l = female_last[rnd.Next(female_last.Length)];
        }
        else
        {
          g = Organization.Sex.Male;
          f = male_first[rnd.Next(male_first.Length)];
          m = male_middle[rnd.Next(male_middle.Length)];
          l = male_last[rnd.Next(male_last.Length)];
        }
        Organization.JobTitle t = Organization.JobTitle.Team.TeamMember;
        if (org.AddEmployee(l, f, m, d, g, q.ResultFields(i % teams_max)["ID"] as Organization.SID, t))
          i++;
      }
      Console.WriteLine("Ready");
    }
    static void Main(string[] args)
    {
      Organization my_org;
      string input;
      int cities_max, branches_max, departments_max, teams_max, employees_max;
      bool repeat;

      Console.OutputEncoding = Encoding.Unicode;
      Console.InputEncoding = Encoding.Unicode;

      Console.WriteLine("AnDS_lab02 v1.0");
      Console.Write("Enter the number of cities: ");
      for (cities_max = 1, repeat = true; repeat;)
      {
        int xPos = 0;
        try
        {
          xPos = Console.CursorLeft;
          cities_max = Convert.ToInt32(Console.ReadLine());
          repeat = false;
        }
        catch (FormatException)
        {
          Console.SetCursorPosition(xPos, Console.CursorTop - 1);
          Console.Write("                    ");
          Console.CursorLeft = xPos;
        }
      }
      Console.Write("Enter the number of branches: ");
      for (branches_max = 2, repeat = true; repeat;)
      {
        int xPos = 0;
        try
        {
          xPos = Console.CursorLeft;
          branches_max = Convert.ToInt32(Console.ReadLine());
          repeat = false;
        }
        catch (FormatException)
        {
          Console.SetCursorPosition(xPos, Console.CursorTop - 1);
          Console.Write("                    ");
          Console.CursorLeft = xPos;
        }
      }
      Console.Write("Enter the number of department: ");
      for (departments_max = 3, repeat = true; repeat;)
      {
        int xPos = 0;
        try
        {
          xPos = Console.CursorLeft;
          departments_max = Convert.ToInt32(Console.ReadLine());
          repeat = false;
        }
        catch (FormatException)
        {
          Console.SetCursorPosition(xPos, Console.CursorTop - 1);
          Console.Write("                    ");
          Console.CursorLeft = xPos;
        }
      }
      Console.Write("Enter the number of teams: ");
      for (teams_max = 4, repeat = true; repeat;)
      {
        int xPos = 0;
        try
        {
          xPos = Console.CursorLeft;
          teams_max = Convert.ToInt32(Console.ReadLine());
          repeat = false;
        }
        catch (FormatException)
        {
          Console.SetCursorPosition(xPos, Console.CursorTop - 1);
          Console.Write("                    ");
          Console.CursorLeft = xPos;
        }
      }
      Console.Write("Enter the number of employees: ");
      for (employees_max = 5, repeat = true; repeat;)
      {
        int xPos = 0;
        try
        {
          xPos = Console.CursorLeft;
          employees_max = Convert.ToInt32(Console.ReadLine());
          repeat = false;
        }
        catch (FormatException)
        {
          Console.SetCursorPosition(xPos, Console.CursorTop - 1);
          Console.Write("                    ");
          Console.CursorLeft = xPos;
        }
      }
      branches_max = branches_max < cities_max ? cities_max : branches_max;
      departments_max = departments_max < branches_max ? branches_max : departments_max;
      teams_max = teams_max < departments_max ? departments_max : teams_max;
      employees_max = employees_max < branches_max + departments_max + teams_max ? branches_max + departments_max + teams_max : employees_max;

      Generate(out my_org, cities_max, branches_max, departments_max, teams_max, employees_max);

      List<string> output;
      Organization.IQuery q;
      Random rnd = new Random((int)DateTime.Now.Ticks);
      QueryResolver qr = new QueryResolver(my_org);
      QueryResolver qs = new QueryResolver(my_org);
      Console.WriteLine("Query Resolver v1.0");
      while (true)
      {
        Console.Write("> ");
        input = Console.ReadLine();
        Match sub;
        if (Regex.IsMatch(input, @"^\s*quit\s*$"))
        {
          break;
        }
        else if ((sub = Regex.Match(input, @"^\s*write\s+(?<File>\w+\.txt)\s+(?<Count>\d+)(?:\s+(?<Query>.*))?$")).Success)
        {
          Console.Write("Writing".PadRight(20, '.'));
          try
          {
            StreamWriter sw = new StreamWriter(sub.Groups["File"].Value);
            qs.Resolve("new query [Employee]");
            for (int i = 0; i < Convert.ToInt32(sub.Groups["Count"].Value); i++)
              sw.WriteLine(qs.Resolve($"?[{i}]:ID")[0]);
            sw.Close();
            Console.WriteLine("done");
          }
          catch (QueryResolver.BadQuery)
          {
            Console.WriteLine("Bad Query");
          }
        }
        else if ((sub = Regex.Match(input, @"^\s*read\s+(?<File>\w+\.txt)\s*$")).Success)
        {
          Console.WriteLine("Press any key to continue, <Esc> to abort");
          StreamReader sr = new StreamReader(sub.Groups["File"].Value);
          string line;
          while ((line = sr.ReadLine()) != null)
          {
            try
            {
              output = qs.Resolve($"lookup {{{line}}} ?");
              for (int i = 0; i < output.Count; i++)
                Console.WriteLine(output[i]);
              if (Console.ReadKey(true).Key == ConsoleKey.Escape)
                break;
            }
            catch (QueryResolver.BadQuery)
            {
              Console.WriteLine("Bad Query");
            }
          }
          sr.Close();
        }
        else
        {
          try
          {
            output = qr.Resolve(input);
            for (int i = 0; i < output.Count; i++)
              Console.WriteLine(output[i]);
          }
          catch (QueryResolver.BadQuery)
          {
            Console.WriteLine("Bad Query");
          }
        }
      }
    }
  }
}
