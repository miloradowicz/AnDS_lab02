﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataTypes
{
  public interface IMyImmutable { }
  public interface IMyList<T>
  {
    void Prepend(T item);
    void InsertAt(int index, T item);
    void DeleteHead();
    void DeleteAt(int index);
    T ElementAt(int index);
    int IndexOf(T item);
    int Find(Predicate<T> predicate);
    List<int> FindAll(Predicate<T> predicate);
    bool Contains(T item);
    void ForEach(Action<T> action);
    int Count { get; }
    T this[int i] { get; }
  }
  public sealed class MyListSingle<T> : IMyList<T>, IEnumerable
  {
    public class ListIsEmpty : Exception { }
    public class BadIndex : Exception { }

    internal sealed class Node
    {
      Node _next;
      readonly T _item;

      internal Node(Node nextNode, T item)
      {
        _next = nextNode;
        _item = item;
      }
      public override bool Equals(object obj)
      {
        if (obj == null)
          return false;
        else if (obj.GetType() != GetType())
          return false;
        else
          return (obj as Node)._item.Equals(_item);
      }
      public bool Equals(T item)
      {
        if (item == null)
          return false;
        else
          return item.Equals(_item);
      }
      public override int GetHashCode()
      {
        return _item.GetHashCode();
      }

      internal Node NextNode
      {
        get { return _next; }
        set { _next = value; }
      }
      internal T Item
      {
        get { return _item; }
      }
    }
    public sealed class MyListEnumerator : IEnumerator
    {
      readonly Node _head;
      Node _currentNode;

      internal MyListEnumerator(Node head)
      {
        _head = head;
        _currentNode = _head;
      }
      public bool MoveNext()
      {
        if (_currentNode == null)
          return false;
        else if (_currentNode == _head)
          return true;
        else
          return (_currentNode = _currentNode.NextNode) != null;
      }
      public void Reset()
      {
        _currentNode = _head;
      }
      object IEnumerator.Current
      {
        get { return Current; }
      }
      public T Current
      {
        get { return _currentNode.Item; }
      }
    }

    Node _head;
    Node _tail;
    int _count;

    public MyListSingle()
    {
      _head = null;
      _tail = null;
      _count = 0;
    }
    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    public MyListEnumerator GetEnumerator()
    {
      return new MyListEnumerator(_head);
    }

    public void Prepend(T item)
    {
      if (_head == null)
        _head = _tail = new Node(null, item);
      else
        _head = new Node(_head, item);
      _count++;
    }
    public void InsertAt(int index, T item)
    {
      if (index < 0 || index > _count)
        throw new BadIndex();
      else if (_head == null)
        _head = _tail = new Node(null, item);
      else
      {
        Node q = null, p = _head;
        for (; index > 0; index--, q = p, p = p.NextNode) { }
        if (q == null)
          _head = new Node(_head, item);
        else
        {
          q.NextNode = new Node(p, item);
          if (p == null)
            _tail = q.NextNode;
        }
      }
    }
    public void DeleteHead()
    {
      if (_head == null)
        throw new ListIsEmpty();
      else
      {
        _head = _head.NextNode;
        if (_head == null)
          _tail = null;
        _count--;
      }
    }
    public void DeleteAt(int index)
    {
      if (index < 0 || index >= _count)
        throw new ListIsEmpty();
      else
      {
        Node q = null, p = _head;
        for (; index > 0; index--, q = p, p = p.NextNode) { }
        if (q == null)
          _head = p.NextNode;
        else
          q.NextNode = p.NextNode;
        if (p.NextNode == null)
          _tail = q;
        _count--;
      }
    }
    public T ElementAt(int index)
    {
      if (index < 0 || index >= _count)
        throw new BadIndex();
      else
      {
        Node p = _head;
        for (; index > 0; index--, p = p.NextNode) { }
        return p.Item;
      }
    }
    public int IndexOf(T item)
    {
      Node p = _head;
      for (int i = 0; p != null; i++, p = p.NextNode)
      {
        if (p.Item.Equals(item))
          return i;
      }
      return -1;
    }
    public int Find(Predicate<T> predicate)
    {
      Node p = _head;
      for (int i = 0; p != null; i++, p = p.NextNode)
      {
        if (predicate(p.Item))
          return i;
      }
      return -1;
    }
    public List<int> FindAll(Predicate<T> predicate)
    {
      List<int> r = new List<int> { };
      Node p = _head;
      for (int i = 0; p != null; i++, p = p.NextNode)
      {
        if (predicate(p.Item))
          r.Add(i);
      }
      return r;
    }
    public bool Contains(T item)
    {
      for (Node p = _head; p != null; p = p.NextNode)
      {
        if (p.Item.Equals(item))
          return true;
      }
      return false;
    }
    public void ForEach(Action<T> action)
    {
      for (Node p = _head; p != null; p = p.NextNode)
        action(p.Item);
    }

    public int Count
    {
      get { return _count; }
    }
    public T this[int index]
    {
      get { return ElementAt(index); }
    }
  }
  public interface IMyHashTable<TKey, TValue>
  {
    void Add(TKey key, TValue value);
    void Delete(TKey key);
    //TKey Find(Predicate<TValue> predicate);
    List<TKey> FindAll(Predicate<TValue> predicate);
    TValue Get(TKey key);
    bool ContainsKey(TKey key);
    int Count { get; }
    TValue this[TKey key] { get; }
  }
  public abstract class MyHashTable<TKey, TValue> : IMyHashTable<TKey, TValue>, IEnumerable
  {
    public class SizeTooBig : Exception { }
    public class HashTableEmpty : Exception { }
    public class NoSuchKey : Exception { }
    public class KeyAlreadyExists : Exception { }

    protected const int defaultSize = 16;
    protected const int maxSize = (int.MaxValue >> 1) - (int.MaxValue >> 2);
    protected const double maxLoadFactor = 0.75;
    protected const int prime = 1303;

    public sealed class MyKeyValuePair
    {
      public class BadKey : Exception { }

      readonly TKey _key;
      readonly TValue _value;

      public MyKeyValuePair(TKey key, TValue value)
      {
        _key = key;
        _value = value;
        if (key == null)
          throw new BadKey();
      }
      public override bool Equals(object obj)
      {
        if (obj == null)
          return false;
        else if (obj.GetType() != GetType())
          return false;
        else
          return (obj as MyKeyValuePair)._key.Equals(_key) && (obj as MyKeyValuePair)._value.Equals(_value);
      }
      public override int GetHashCode()
      {
        return _key.GetHashCode();
      }

      public TKey Key
      {
        get { return _key; }
      }
      public TValue Value
      {
        get { return _value; }
      }
    }

    protected static int HornerHash(string s, int p, int m)
    {
      long r = 0;
      for (int i = 0; i < s.Length; i++)
        r = (r * p + s[i]) % m;
      return (int)r;
    }

    protected int _count;
    protected int _size;

    protected MyHashTable(int count, int maxSize, int defaultSize, double maxLoadFactor)
    {
      _count = 0;
      if (count >= maxSize)
        throw new SizeTooBig();
      else if (count < defaultSize)
        _size = defaultSize;
      else
      {
        int b = int.MaxValue - (int.MaxValue >> 1);
        for (b >>= 2; count < maxLoadFactor * b; b >>= 1) { }
        _size = b << 1;
      }
    }
    IEnumerator IEnumerable.GetEnumerator()
    {
      return _getEnumerator();
    }
    protected abstract IEnumerator _getEnumerator();
    protected int Hash(TKey key)
    {
      return HornerHash(key.ToString(), prime, _size);
    }
    protected abstract void Resize();

    public abstract void Add(TKey key, TValue value);
    public abstract void Delete(TKey key);
    //public abstract TKey Find(Predicate<TValue> predicate);
    public abstract List<TKey> FindAll(Predicate<TValue> predicate);
    public abstract TValue Get(TKey key);
    public abstract bool ContainsKey(TKey key);

    double LoadFactor
    {
      get
      {
        return (double)_count / _size;
      }
    }
    public int Count
    {
      get { return _count; }
    }
    public TValue this[TKey key]
    {
      get { return Get(key); }
    }
  }
  public sealed class MyHashTableChaining<TKey, TValue> : MyHashTable<TKey, TValue>
    where TKey : IMyImmutable
    where TValue : IMyImmutable
  {
    new const double maxLoadFactor = 1.25;

    public sealed class MyHashTableEnumerator : IEnumerator
    {
      readonly int _count;
      readonly MyListSingle<MyKeyValuePair>[] _buckets;
      int _position;
      int _index;
      IEnumerator _listEnumerator;

      internal MyHashTableEnumerator(int count, MyListSingle<MyKeyValuePair>[] buckets)
      {
        _count = count;
        _buckets = buckets;
        _position = -1;
        _index = 0;
        _listEnumerator = buckets[0].GetEnumerator();
      }
      public bool MoveNext()
      {
        _position++;
        if (_position >= _count)
          return false;
        else
        {
          while (!_listEnumerator.MoveNext())
            _listEnumerator = _buckets[++_index].GetEnumerator();
          return true;
        }
      }
      public void Reset()
      {
        _position = -1;
        _index = 0;
      }
      object IEnumerator.Current
      {
        get { return Current; }
      }
      public MyKeyValuePair Current
      {
        get
        {
          return _listEnumerator.Current as MyKeyValuePair;
        }
      }
    }

    MyListSingle<MyKeyValuePair>[] _buckets;

    public MyHashTableChaining() : this(defaultSize) { }
    public MyHashTableChaining(int count) : base(count, maxSize, defaultSize, maxLoadFactor)
    {
      _buckets = new MyListSingle<MyKeyValuePair>[_size];
      for (int i = 0; i < _buckets.Length; i++)
        _buckets[i] = new MyListSingle<MyKeyValuePair>();
    }
    public MyHashTableEnumerator GetEnumerator()
    {
      return new MyHashTableEnumerator(_count, _buckets);
    }
    protected override IEnumerator _getEnumerator()
    {
      return GetEnumerator();
    }
    protected override void Resize()
    {
      MyListSingle<MyKeyValuePair>[] b = _buckets;
      _buckets = new MyListSingle<MyKeyValuePair>[_size *= 2];
      _count = 0;
      Action<MyKeyValuePair> a = new Action<MyKeyValuePair>(arg => Add(arg.Key, arg.Value));
      for (int i = 0; i < b.Length; i++)
        b[i].ForEach(a);
    }

    public override void Add(TKey key, TValue value)
    {
      MyListSingle<MyKeyValuePair> b = _buckets[Hash(key)];
      if (b.Find(arg => arg.Key.Equals(key)) >= 0)
        throw new KeyAlreadyExists();
      else
      {
        if (_count + 1 > maxLoadFactor * _size)
          Resize();

        b.Prepend(new MyKeyValuePair(key, value));
        _count++;
      }
    }
    public override void Delete(TKey key)
    {
      if (_count == 0)
        throw new HashTableEmpty();

      MyListSingle<MyKeyValuePair> b = _buckets[Hash(key)];
      int i = b.Find(arg => arg.Key.Equals(key));
      if (i < 0)
        throw new NoSuchKey();
      else
      {
        b.DeleteAt(i);
        _count--;
      }
    }
    //public override TKey Find(Predicate<TValue> predicate)
    //{
    //  if (_count == 0)
    //    return null;

    //  Predicate<MyKeyValuePair> p = new Predicate<MyKeyValuePair>(arg => predicate(arg.Value));
    //  for (int i = 0, j; i < _buckets.Length; i++)
    //  {
    //    if ((j = _buckets[i].Find(p)) >= 0)
    //      return _buckets[i][j].Key;
    //  }
    //  return null;
    //}
    public override List<TKey> FindAll(Predicate<TValue> predicate)
    {
      List<TKey> r = new List<TKey> { };
      Predicate<MyKeyValuePair> p = new Predicate<MyKeyValuePair>(arg => predicate(arg.Value));
      for (int i = 0; i < _buckets.Length; i++)
      {
        List<int> l = _buckets[i].FindAll(p);
        for (int j = 0; j < l.Count; j++)
          r.Add(_buckets[i][l[j]].Key);
      }
      return r;
    }
    public override TValue Get(TKey key)
    {
      if (_count == 0)
        throw new HashTableEmpty();

      MyListSingle<MyKeyValuePair> b = _buckets[Hash(key)];
      int i = b.Find(arg => arg.Key.Equals(key));
      if (i >= 0)
        return b[i].Value;
      else
        throw new NoSuchKey();
    }
    public override bool ContainsKey(TKey key)
    {
      if (_count == 0)
        return false;

      MyListSingle<MyKeyValuePair> b = _buckets[Hash(key)];
      if (b.Find(arg => arg.Key.Equals(key)) < 0)
        return false;
      else
        return true;
    }
  }
  public sealed class MyHashTableOpen<TKey, TValue> : MyHashTable<TKey, TValue>
    where TKey : IMyImmutable
    where TValue : IMyImmutable
  {
    new const double maxLoadFactor = 0.75;
    const int prime2 = 1789;

    public sealed class MyHashTableEnumerator : IEnumerator
    {
      readonly int _count;
      readonly object[] _buckets;
      int _position;
      int _index;

      internal MyHashTableEnumerator(int count, object[] buckets)
      {
        _count = count;
        _buckets = buckets;
        _position = -1;
        _index = 0;
      }
      public bool MoveNext()
      {
        _position++;
        if (_position >= _count)
          return false;
        else
        {
          for (; !(_buckets[_index] is MyKeyValuePair); _index++) { }
          return true;
        }
      }
      public void Reset()
      {
        _position = -1;
        _index = 0;
      }
      object IEnumerator.Current
      {
        get { return Current; }
      }
      public MyKeyValuePair Current
      {
        get
        {
          return _buckets[_index] as MyKeyValuePair;
        }
      }
    }
    class Deleted { }

    static readonly Deleted DELETED = new Deleted();

    object[] _buckets;

    public MyHashTableOpen(int count) : base(count, maxSize, defaultSize, maxLoadFactor)
    {
      _buckets = new object[_size];
    }
    public MyHashTableEnumerator GetEnumerator()
    {
      return new MyHashTableEnumerator(_count, _buckets);
    }
    protected override IEnumerator _getEnumerator()
    {
      return GetEnumerator();
    }
    int Hash2(TKey key)
    {
      return prime2 - HornerHash(key.ToString(), prime, prime2);
    }
    protected override void Resize()
    {
      object[] _b = _buckets;
      _buckets = new object[_size *= 2];
      for (int i = 0; i < _b.Length; i++)
      {
        if (_b[i] is MyKeyValuePair t)
        {
          int h = Hash(t.Key);
          int h2 = Hash2(t.Key);
          while (_buckets[h] != null)
            h = (h + h2) % _size;
          _buckets[h] = t;
        }
      }
    }

    public override void Add(TKey key, TValue value)
    {
      int h = Hash(key);
      int h2 = Hash2(key);
      while (_buckets[h] is MyKeyValuePair t)
      {
        if (t.Key.Equals(key))
          throw new KeyAlreadyExists();
        else
          h = (h + h2) % _size;
      }

      if (_count + 1 > maxLoadFactor * _size)
        Resize();

      _buckets[h] = new MyKeyValuePair(key, value);
      _count++;
    }
    public override void Delete(TKey key)
    {
      if (_count == 0)
        throw new HashTableEmpty();

      int h = Hash(key);
      int h2 = Hash2(key);
      for (int i = 0; i < _buckets.Length && _buckets[h] != null; i++)
      {
        if (_buckets[h] is MyKeyValuePair t && t.Key.Equals(key))
        {
          _buckets[h] = DELETED;
          _count--;
        }
        else
          h = (h + h2) % _size;
      }
      throw new NoSuchKey();
    }
    //public override TKey Find(Predicate<TValue> predicate)
    //{
    //  if (_count == 0)
    //    return null;

    //  for (int i = 0; i < _buckets.Length; i++)
    //  {
    //    if (_buckets[i] is MyKeyValuePair t && predicate(t.Value))
    //      return t.Key;
    //  }
    //  return null;
    //}
    public override List<TKey> FindAll(Predicate<TValue> predicate)
    {
      List<TKey> r = new List<TKey> { };
      for (int i = 0; i < _buckets.Length; i++)
      {
        if (_buckets[i] is MyKeyValuePair t && predicate(t.Value))
          r.Add(t.Key);
      }
      return r;
    }
    public override TValue Get(TKey key)
    {
      if (_count == 0)
        throw new HashTableEmpty();

      int h = Hash(key);
      int h2 = Hash2(key);
      for (int i = 0; i < _buckets.Length && _buckets[h] != null; i++)
      {
        if (_buckets[h] is MyKeyValuePair t && t.Key.Equals(key))
          return t.Value;
        else
          h = (h + h2) % _size;
      }
      throw new NoSuchKey();
    }
    public override bool ContainsKey(TKey key)
    {
      if (_count == 0)
        return false;

      int h = Hash(key);
      int h2 = Hash2(key);
      for (int i = 0; i < _buckets.Length && _buckets[h] != null; i++)
      {
        if (_buckets[h] is MyKeyValuePair t && t.Key.Equals(key))
          return true;
        else
          h = (h + h2) % _size;
      }
      return false;
    }
  }
}
