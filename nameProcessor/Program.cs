﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace nameProcessor
{
  class Program
  {
    static void Main(string[] args)
    {
      List<String> lastNames, firstNames, middleNames, branches;

      StreamReader sr;
      StreamWriter swl, swf, swm, sw;
      String line;
      String[] nameSplit;


      // male names
      lastNames = new List<String> { };
      firstNames = new List<String> { };
      middleNames = new List<String> { };

      try
      {
        sr = new StreamReader("male_last.txt");
        while ((line = sr.ReadLine()) != null)
        {
          line = line.Trim(' ', '\r', '\n');
          if (line.Length > 0)
            lastNames.Add(line);
        }
        sr.Close();

        sr = new StreamReader("male_first.txt");
        while ((line = sr.ReadLine()) != null)
        {
          line = line.Trim(' ', '\r', '\n');
          if (line.Length > 0)
            firstNames.Add(line);
        }
        sr.Close();

        sr = new StreamReader("male_middle.txt");
        line = sr.ReadLine();
        while ((line = sr.ReadLine()) != null)
        {
          line = line.Trim(' ', '\r', '\n');
          if (line.Length > 0)
            middleNames.Add(line);
        }
        sr.Close();
      }
      catch (Exception e) { }

      sr = new StreamReader("male.txt");
      swl = new StreamWriter("male_last.txt", true);
      swf = new StreamWriter("male_first.txt", true);
      swm = new StreamWriter("male_middle.txt", true);

      while ((line = sr.ReadLine()) != null)
      {
        line = line.Trim(' ', '\r', '\n');
        nameSplit = line.Split(' ');

        if (!lastNames.Contains(nameSplit[0]) && nameSplit[0].Length > 0)
        {
          lastNames.Add(nameSplit[0]);
          swl.WriteLine(nameSplit[0]);
        }

        if (!firstNames.Contains(nameSplit[1]) && nameSplit[1].Length > 0)
        {
          firstNames.Add(nameSplit[1]);
          swf.WriteLine(nameSplit[1]);
        }

        if (!middleNames.Contains(nameSplit[2]) && nameSplit[2].Length > 0)
        {
          middleNames.Add(nameSplit[2]);
          swm.WriteLine(nameSplit[2]);
        }
      }
      sr.Close();
      swl.Close();
      swf.Close();
      swm.Close();


      // female names
      lastNames = new List<String> { };
      firstNames = new List<String> { };
      middleNames = new List<String> { };

      try
      {
        sr = new StreamReader("female_last.txt");
        while ((line = sr.ReadLine()) != null)
        {
          line = line.Trim(' ', '\r', '\n');
          if (line.Length > 0)
            lastNames.Add(line);
        }
        sr.Close();

        sr = new StreamReader("female_first.txt");
        while ((line = sr.ReadLine()) != null)
        {
          line = line.Trim(' ', '\r', '\n');
          if (line.Length > 0)
            firstNames.Add(line);
        }
        sr.Close();

        sr = new StreamReader("female_middle.txt");
        line = sr.ReadLine();
        while ((line = sr.ReadLine()) != null)
        {
          line = line.Trim(' ', '\r', '\n');
          if (line.Length > 0)
            middleNames.Add(line);
        }
        sr.Close();
      }
      catch (Exception e) { }

      sr = new StreamReader("female.txt");
      swl = new StreamWriter("female_last.txt", true);
      swf = new StreamWriter("female_first.txt", true);
      swm = new StreamWriter("female_middle.txt", true);

      while ((line = sr.ReadLine()) != null)
      {
        line = line.Trim(' ', '\r', '\n');
        nameSplit = line.Split(' ');

        if (!lastNames.Contains(nameSplit[0]) && nameSplit[0].Length > 0)
        {
          lastNames.Add(nameSplit[0]);
          swl.WriteLine(nameSplit[0]);
        }

        if (!firstNames.Contains(nameSplit[1]) && nameSplit[1].Length > 0)
        {
          firstNames.Add(nameSplit[1]);
          swf.WriteLine(nameSplit[1]);
        }

        if (!middleNames.Contains(nameSplit[2]) && nameSplit[2].Length > 0)
        {
          middleNames.Add(nameSplit[2]);
          swm.WriteLine(nameSplit[2]);
        }
      }
      sr.Close();
      swl.Close();
      swf.Close();
      swm.Close();


      // branches names
      branches = new List<String> { };

      try
      {
        sr = new StreamReader("branches.txt");
        while ((line = sr.ReadLine()) != null)
        {
          line = line.Trim(' ', '\r', '\n');
          if (line.Length > 0)
            branches.Add(line);
        }
        sr.Close();
      }
      catch (Exception e) { }

      sr = new StreamReader("branches_unprocessed.txt");
      sw = new StreamWriter("branches.txt", true);

      while ((line = sr.ReadLine()) != null)
      {
        line = line.Trim(' ', '\r', '\n');

        if (!branches.Contains(line) && line.Length > 0)
        {
          branches.Add(line);
          sw.WriteLine(Char.ToUpper(line[0]) + line.Substring(1));
        }
      }
      sr.Close();
      sw.Close();
    }
  }
}
