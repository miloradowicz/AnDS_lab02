﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MyDataTypes;
using static MyOrganization.Organization;

namespace MyOrganization
{
  internal sealed class MyOrgHashTable : MyHashTable<SID, IRanked>
  {
    new const double maxLoadFactor = 0.75;
    const int prime2 = 1789;

    class Deleted { }
    public sealed class MyHashTableEnumerator : IEnumerator
    {
      readonly int _count;
      readonly object[] _buckets;
      int _position;
      int _index;

      internal MyHashTableEnumerator(int count, object[] buckets)
      {
        _count = count;
        _buckets = buckets;
        _position = -1;
        _index = 0;
      }
      public bool MoveNext()
      {
        _position++;
        if (_position >= _count)
          return false;
        else
        {
          for (; !(_buckets[_index] is IRanked); _index++) { }
          return true;
        }
      }
      public void Reset()
      {
        _position = -1;
        _index = 0;
      }
      object IEnumerator.Current
      {
        get { return Current; }
      }
      public IRanked Current
      {
        get
        {
          return _buckets[_index] as IRanked;
        }
      }
    }

    static readonly Deleted DELETED = new Deleted();

    object[] _buckets;

    public MyOrgHashTable(int count) : base(count, maxSize, defaultSize, maxLoadFactor)
    {
      _buckets = new object[_size];
    }
    public MyHashTableEnumerator GetEnumerator()
    {
      return new MyHashTableEnumerator(_count, _buckets);
    }
    protected override IEnumerator _getEnumerator()
    {
      return GetEnumerator();
    }
    int Hash2(SID key)
    {
      return prime2 - HornerHash(key.ToString(), prime, prime2);
    }
    protected override void Resize()
    {
      object[] _b = _buckets;
      _buckets = new object[_size *= 2];
      for (int i = 0; i < _b.Length; i++)
      {
        if (_b[i] is IRanked t)
        {
          int h = Hash(t.ID);
          int h2 = Hash2(t.ID);
          while (_buckets[h] != null)
            h = (h + h2) % _size;
          _buckets[h] = t;
        }
      }
    }

    public override void Add(SID key, IRanked value)
    {
      int h = Hash(key);
      int h2 = Hash2(key);
      while (_buckets[h] is IRanked t)
      {
        if (t.ID.Equals(key))
          throw new KeyAlreadyExists();
        else
          h = (h + h2) % _size;
      }

      if (_count + 1 > maxLoadFactor * _size)
        Resize();

      _buckets[h] = value;
      _count++;
    }
    public override void Delete(SID key)
    {
      if (_count == 0)
        throw new HashTableEmpty();

      int h = Hash(key);
      int h2 = Hash2(key);
      for (int i = 0; i < _buckets.Length && _buckets[h] != null; i++)
      {
        if (_buckets[h] is IRanked t && t.ID.Equals(key))
        {
          _buckets[h] = DELETED;
          _count--;
        }
        else
          h = (h + h2) % _size;
      }
      throw new NoSuchKey();
    }
    public override List<SID> FindAll(Predicate<IRanked> predicate)
    {
      List<SID> r = new List<SID> { };
      for (int i = 0; i < _buckets.Length; i++)
      {
        if (_buckets[i] is IRanked t && predicate(t))
          r.Add(t.ID);
      }
      return r;
    }
    public override IRanked Get(SID key)
    {
      if (_count == 0)
        throw new HashTableEmpty();

      int h = Hash(key);
      int h2 = Hash2(key);
      for (int i = 0; i < _buckets.Length && _buckets[h] != null; i++)
      {
        if (_buckets[h] is IRanked t && t.ID.Equals(key))
          return t;
        else
          h = (h + h2) % _size;
      }
      throw new NoSuchKey();
    }
    public override bool ContainsKey(SID key)
    {
      if (_count == 0)
        return false;

      int h = Hash(key);
      int h2 = Hash2(key);
      for (int i = 0; i < _buckets.Length && _buckets[h] != null; i++)
      {
        if (_buckets[h] is IRanked t && t.ID.Equals(key))
          return true;
        else
          h = (h + h2) % _size;
      }
      return false;
    }
    //public SID Find(Predicate<IRanked> predicate)
    //{
    //  if (_count == 0)
    //    return null;

    //  for (int i = 0; i < _buckets.Length; i++)
    //  {
    //    if (_buckets[i] is IRanked t && predicate(t))
    //      return t.ID;
    //  }
    //  return null;
    //}
    public List<IRanked> FindAllVal(Predicate<IRanked> predicate)
    {
      List<IRanked> r = new List<IRanked> { };
      for (int i = 0; i < _buckets.Length; i++)
      {
        if (_buckets[i] is IRanked t && predicate(t))
          r.Add(t);
      }
      return r;
    }
  }
  public interface IOrganization
  {
    bool AddCity(string name);
    bool AddBranch(string name, SID headOffice);
    bool AddDepartment(string name, SID headOffice);
    bool AddTeam(string name, SID headOffice);
    bool AddEmployee(string lastName, string firstName, string middleName, DateTime dob, Sex sex, SID office, JobTitle jobTitle);
    IQuery GetQuery();
    IQuery GetCities();
    IQuery GetBranches();
    IQuery GetDepartments();
    IQuery GetTeams();
    IQuery GetEmployees();
  }
  public class Organization : IOrganization
  {
    public class RogueUnit : Exception { }
    public class BadAssignment : Exception { }
    public class BadJobTitle : Exception { }

    public enum Sex
    {
      Unspecified = 0,
      Female = 1,
      Male = 2
    }
    public abstract class JobTitle
    {
      public static class Role
      {
        public interface Management { }
        public interface Regular { }
      }
      public static class Level
      {
        public interface UpperManagement : Role.Management { }
        public interface MiddleManagement : Role.Management { }
        public interface LowerManagement : Role.Management { }
      }
      public static class Scope
      {
        public interface Branch { }
        public interface Department { }
        public interface Team { }
      }
      public interface BranchDirector : Role.Management, Level.MiddleManagement, Scope.Branch { }
      public interface DepartmentHead : Role.Management, Level.MiddleManagement, Scope.Department { }
      public interface TeamLead : Role.Management, Level.LowerManagement, Scope.Team { }
      public interface TeamMember : Role.Regular, Scope.Team { }
      public static class Branch
      {
        class BranchDirectorJob : JobTitle, BranchDirector
        {
          internal BranchDirectorJob() { }
          public override string ToString() { return "Branch Director"; }
        }
        public static readonly JobTitle BranchDirector = new BranchDirectorJob();
      }
      public static class Department
      {
        class DepartmentHeadJob : JobTitle, DepartmentHead
        {
          internal DepartmentHeadJob() { }
          public override string ToString() { return "Department Head"; }
        }
        public static readonly JobTitle DepartmentHead = new DepartmentHeadJob();
      }
      public static class Team
      {
        class TeamLeadJob : JobTitle, TeamLead
        {
          internal TeamLeadJob() { }
          public override string ToString() { return "Team Lead"; }
        }
        class TeamMemberJob : JobTitle, TeamMember
        {
          internal TeamMemberJob() { }
          public override string ToString() { return "Team Member"; }
        }
        public static readonly JobTitle TeamLead = new TeamLeadJob();
        public static readonly JobTitle TeamMember = new TeamMemberJob();
      }
    }
    public sealed class SID : IMyImmutable
    {
      readonly string _sid;

      public SID(string id)
      {
        _sid = id;
      }
      public override string ToString()
      {
        return _sid;
      }
      public override bool Equals(object obj)
      {
        if (obj == null)
          return false;
        else if (obj.GetType() != GetType())
          return false;
        else
          return ((obj as SID)._sid.Equals(_sid));
      }
      public override int GetHashCode()
      {
        return ToString().GetHashCode();
      }

      public string Key
      {
        get { return ToString(); }
      }
    }
    internal interface IEntity : IMyImmutable
    {
      SID ID { get; }
      string Name { get; }
      Dictionary<string, object> Fields { get; }
    }
    internal interface IRanked : IEntity
    {
      bool BelongsTo(IRanked ranked);
      int Rank { get; }
    }
    abstract class Unit : IRanked
    {
      readonly string _name;
      readonly Unit _headOffice;

      protected Unit(string name, Unit headOffice)
      {
        _name = name;
        _headOffice = headOffice;
      }
      public bool BelongsTo(IRanked office)
      {
        if (office.Rank > Rank)
          return false;
        else if (office.Rank == Rank && !office.Equals(this))
          return false;
        else if (office.Rank == Rank)
          return true;
        else
          return _headOffice.BelongsTo(office);

      }
      public override string ToString()
      {
        return (_headOffice != null) ? $"{_headOffice.ToString()} > {_name}" : _name;
      }
      public override bool Equals(object obj)
      {
        if (obj == null)
          return false;
        else if (obj.GetType() != GetType())
          return false;
        else if (!(obj as Unit)._name.Equals(_name))
          return false;
        else if ((obj as Unit)._headOffice == null && _headOffice != null)
          return false;
        else if ((obj as Unit)._headOffice == null && _headOffice == null)
          return true;
        else
          return (obj as Unit)._headOffice.Equals(_headOffice);
      }
      public override int GetHashCode()
      {
        return ToString().GetHashCode();
      }

      public virtual SID ID
      {
        get { return new SID(ToString()); }
      }
      public abstract Dictionary<string, object> Fields { get; }
      public string Name
      {
        get { return _name; }
      }
      public Unit HO
      {
        get { return _headOffice; }
      }
      public abstract int Rank { get; }
    }
    sealed class City : Unit
    {
      internal City(string name) : base(name, null) { }
      public override Dictionary<string, object> Fields
      {
        get { return new Dictionary<string, object> { { "ID", ID }, { "Name", Name } }; }
      }
      public override int Rank
      {
        get { return 0; }
      }
    }
    sealed class Branch : Unit
    {
      internal Branch(string name, City headOffice) : base(name, headOffice)
      {
        if (headOffice == null)
          throw new RogueUnit();
      }
      public override Dictionary<string, object> Fields
      {
        get { return new Dictionary<string, object> { { "ID", ID }, { "Name", Name }, { "City", HO } }; }
      }
      public override int Rank
      {
        get { return 1; }
      }
    }
    sealed class Department : Unit
    {
      internal Department(string name, Branch headOffice) : base(name, headOffice)
      {
        if (headOffice == null)
          throw new RogueUnit();
      }
      public override Dictionary<string, object> Fields
      {
        get { return new Dictionary<string, object> { { "ID", ID }, { "Name", Name }, { "City", HO.HO }, { "Branch", HO } }; }
      }
      public override int Rank
      {
        get { return 2; }
      }
    }
    sealed class Team : Unit
    {
      internal Team(string name, Department headOffice) : base(name, headOffice)
      {
        if (headOffice == null)
          throw new RogueUnit();
      }
      public override Dictionary<string, object> Fields
      {
        get { return new Dictionary<string, object> { { "ID", ID }, { "Name", Name }, { "City", HO.HO.HO }, { "Branch", HO.HO }, { "Department", HO } }; }
      }
      public override int Rank
      {
        get { return 3; }
      }
    }
    abstract class Person : IEntity
    {
      readonly string _firstName;
      readonly string _middleName;
      readonly string _lastName;
      readonly DateTime _dob;
      readonly Sex _sex;

      protected Person(string firstName, string middleName, string lastName, DateTime dob, Sex sex)
      {
        _firstName = firstName;
        _middleName = middleName;
        _lastName = lastName;
        _dob = dob.Date;
        _sex = sex;
      }
      public override string ToString()
      {
        return $"{_lastName} {_firstName} {_middleName} | {_dob.Date:dd.MM.yyyy}";
      }
      public override bool Equals(object obj)
      {
        if (obj == null)
          return false;
        else if (obj.GetType() != GetType())
          return false;
        else if (!(obj as Person)._firstName.Equals(_firstName) || !(obj as Person)._middleName.Equals(_middleName) || !(obj as Person)._lastName.Equals(_lastName) || !(obj as Person)._dob.Equals(_dob))
          return false;
        else
          return true;
      }
      public override int GetHashCode()
      {
        return ToString().GetHashCode();
      }

      public SID ID
      {
        get { return new SID(ToString()); }
      }
      public abstract Dictionary<string, object> Fields { get; }
      public string Name
      {
        get { return $"{_lastName} {_firstName} {_middleName}"; }
      }
      public string FirstName
      {
        get { return _firstName; }
      }
      public string MiddleName
      {
        get { return _middleName; }
      }
      public string LastName
      {
        get { return _lastName; }
      }
      public DateTime DoB
      {
        get { return _dob; }
      }
      public Sex Sex
      {
        get { return _sex; }
      }
    }
    sealed class Employee : Person, IRanked
    {
      readonly Unit _office;
      readonly JobTitle _jobTitle;

      internal Employee(string firstName, string middleName, string lastName, DateTime dob, Sex sex, Unit office, JobTitle jobTitle) : base(firstName, middleName, lastName, dob, sex)
      {
        bool isBranchDirector = office is Branch && jobTitle == JobTitle.Branch.BranchDirector;
        bool isDepartmentHead = office is Department && jobTitle == JobTitle.Department.DepartmentHead;
        bool isTeamLead = office is Team && jobTitle == JobTitle.Team.TeamLead;
        bool isTeamMember = office is Team && jobTitle == JobTitle.Team.TeamMember;

        if (office == null || office is City)
          throw new BadAssignment();
        else if (!isBranchDirector && !isDepartmentHead && !isTeamLead && !isTeamMember)
          throw new BadJobTitle();
        else
        {
          _office = office;
          _jobTitle = jobTitle;
        }
      }
      public override Dictionary<string, object> Fields
      {
        get { return new Dictionary<string, object> { { "ID", ID }, { "Name", Name }, { "DoB", DoB }, { "Sex", Sex }, { "Office", Office }, { "JobTitle", JobTitle } }; }
      }
      public bool BelongsTo(IRanked office)
      {
        return _office.BelongsTo(office);
      }
      public Unit Office
      {
        get { return _office; }
      }
      public JobTitle JobTitle
      {
        get { return _jobTitle; }
      }
      public int Rank
      {
        get { return 5; }
      }
    }
    public interface IQuery
    {
      IQuery Copy();
      IQuery Where(SID id);
      IQuery Where(JobTitle jobTitle);
      IQuery Where(Query.Column column, string name);
      IQuery Select(Query.Column column);
      int ResultsCount();
      ReadOnlyCollection<SID> Results();
      Dictionary<string, object> ResultFields(int index);
    }
    public sealed class Query : IQuery
    {
      public class BadQuery : Exception { }
      public class NoResult : Exception { }

      public enum Column : int
      {
        City = 0,
        Branch = 1,
        Department = 2,
        Team = 3,
        Employee = 15,
        DoB = 16,
        Sex = 17,
        JobTitle = 27
      }

      Organization _org;
      List<IRanked> _results;

      public static IQuery Lookup(Organization organization, SID id)
      {
        IRanked f = organization.Fetch(id);
        if (f != null)
          return new Query { _org = organization, _results = new List<IRanked> { f } };
        else
          throw new NoResult();
      }

      Query() { }
      internal Query(Organization organization)
      {
        _org = organization;
        _results = _org.FindAllVal(arg => arg is City);
      }
      internal Query(Organization organization, Column column)
      {
        _org = organization;
        switch (column)
        {
          case Column.City:
          default:
            _results = _org.FindAllVal(arg => arg is City);
            break;
          case Column.Branch:
            _results = _org.FindAllVal(arg => arg is Branch);
            break;
          case Column.Department:
            _results = _org.FindAllVal(arg => arg is Department);
            break;
          case Column.Team:
            _results = _org.FindAllVal(arg => arg is Team);
            break;
          case Column.Employee:
            _results = _org.FindAllVal(arg => arg is Employee);
            break;
        }
      }
      public IQuery Copy()
      {
        return new Query { _org = this._org, _results = new List<IRanked>(this._results) };
      }
      public IQuery Where(SID id)
      {
        List<IRanked> r = new List<IRanked> { };

        IRanked f = _org.Fetch(id);
        if (f is Unit)
        {
          for (int i = 0; i < _results.Count; i++)
          {
            if (_results[i].BelongsTo(f))
              r.Add(_results[i]);
          }
          _results = r;
          return this;
        }
        else if (f is Employee)
        {
          for (int i = 0; i < _results.Count; i++)
          {
            if (_results[i].Equals(f))
              r.Add(_results[i]);
          }
          _results = r;
          return this;
        }
        else
          return this;
      }
      public IQuery Where(JobTitle jobTitle)
      {
        List<IRanked> r = new List<IRanked> { };
        List<IRanked> s = _org.FindAllVal(arg => arg is Employee t && t.JobTitle == jobTitle);
        for (int j = 0; j < s.Count; j++)
        {
          IRanked f = s[j];
          for (int i = 0; i < _results.Count; i++)
          {
            if (_results[i] is Employee t && t.Equals(f))
              r.Add(_results[i]);
          }
        }
        _results = r;
        return this;
      }
      public IQuery Where(Column column, string name)
      {
        List<IRanked> r = new List<IRanked> { };
        List<IRanked> s;
        switch (column)
        {
          case Column.City:
            s = _org.FindAllVal(arg => arg is City && arg.Name.Equals(name));
            break;
          case Column.Branch:
            s = _org.FindAllVal(arg => arg is Branch && arg.Name.Equals(name));
            break;
          case Column.Department:
            s = _org.FindAllVal(arg => arg is Department && arg.Name.Equals(name));
            break;
          case Column.Team:
            s = _org.FindAllVal(arg => arg is Team && arg.Name.Equals(name));
            break;
          case Column.Employee:
            s = _org.FindAllVal(arg => arg is Employee && arg.Name.Equals(name));
            break;
          default:
            throw new BadQuery();
        }
        for (int j = 0; j < s.Count; j++)
        {
          IRanked f = s[j];
          if (f is Unit)
          {
            for (int i = 0; i < _results.Count; i++)
            {
              if (_results[i].BelongsTo(f))
                r.Add(_results[i]);
            }
          }
          else if (f is Employee)
          {
            for (int i = 0; i < _results.Count; i++)
            {
              if (_results[i].Equals(f))
                r.Add(_results[i]);
            }
          }
          else
            return this;
        }
        _results = r;
        return this;
      }
      public IQuery Select(Column column)
      {
        List<IRanked> r = new List<IRanked> { };
        List<IRanked> s;
        switch (column)
        {
          case Column.City:
          default:
            s = _org._entities.FindAllVal(arg => arg is City);
            break;
          case Column.Branch:
            s = _org._entities.FindAllVal(arg => arg is Branch);
            break;
          case Column.Department:
            s = _org._entities.FindAllVal(arg => arg is Department);
            break;
          case Column.Team:
            s = _org._entities.FindAllVal(arg => arg is Team);
            break;
          case Column.Employee:
            s = _org._entities.FindAllVal(arg => arg is Employee);
            break;
        }
        for (int i = 0; i < _results.Count; i++)
        {
          IRanked f = _results[i];
          for (int j = 0; j < s.Count; j++)
          {
            if (s[j].BelongsTo(f))
              r.Add(s[j]);
          }
        }
        _results = r;
        return this;
      }
      public int ResultsCount()
      {
        return _results.Count;
      }
      public ReadOnlyCollection<SID> Results()
      {
        List<SID> r = new List<SID> { };
        for (int i = 0; i < _results.Count; i++)
          r.Add(_results[i].ID);
        return r.AsReadOnly();
      }
      public Dictionary<string, object> ResultFields(int index)
      {
        if (index < _results.Count)
        {
          IRanked f = _results[index];
          Column c = f is City ? Column.City : f is Branch ? Column.Branch : f is Department ? Column.Department : f is Team ? Column.Team : f is Employee ? Column.Employee : 0;
          Dictionary<string, object> r = f.Fields;
          r.Add("Type", c);
          return r;
        }
        else
          throw new NoResult();
      }
    }

    MyOrgHashTable _entities;

    int _citiesCount, _citiesMax;
    int _branchesCount, _branchesMax;
    int _departmentsCount, _departmentsMax;
    int _teamsCount, _teamsMax;
    int _employeesCount, _employeesMax;

    public Organization(int cities, int branches, int departments, int teams, int employees)
    {
      _entities = new MyOrgHashTable(cities + branches + departments + teams + employees);

      _citiesMax = cities;
      _branchesMax = branches;
      _departmentsMax = departments;
      _teamsMax = teams;
      _employeesMax = employees;
      _citiesCount = 0;
      _branchesCount = 0;
      _departmentsCount = 0;
      _teamsCount = 0;
      _employeesCount = 0;
    }
    public bool AddCity(string name)
    {
      if (_citiesCount < _citiesMax && name != null)
      {
        City t = new City(name);
        try
        {
          _entities.ContainsKey(t.ID);
          _entities.Add(t.ID, t);
          _citiesCount++;
          return true;
        }
        catch (MyOrgHashTable.KeyAlreadyExists)
        {
          return false;
        }
      }
      return false;
    }
    public bool AddBranch(string name, SID headOffice)
    {
      if (_branchesCount < _branchesMax)
      {
        if (Exists(headOffice))
        {
          Branch t = new Branch(name, Fetch(headOffice) as City);
          try
          {
            _entities.ContainsKey(t.ID);
            _entities.Add(t.ID, t);
            _branchesCount++;
            return true;
          }
          catch (MyOrgHashTable.KeyAlreadyExists)
          {
            return false;
          }
        }
      }
      return false;
    }
    public bool AddDepartment(string name, SID headOffice)
    {
      if (_departmentsCount < _departmentsMax)
      {
        if (Exists(headOffice))
        {
          Department t = new Department(name, Fetch(headOffice) as Branch);
          try
          {
            _entities.ContainsKey(t.ID);
            _entities.Add(t.ID, t);
            _departmentsCount++;
            return true;
          }
          catch (MyOrgHashTable.KeyAlreadyExists)
          {
            return false;
          }
        }
      }
      return false;
    }
    public bool AddTeam(string name, SID headOffice)
    {
      if (_teamsCount < _teamsMax)
      {
        if (Exists(headOffice))
        {
          Team t = new Team(name, Fetch(headOffice) as Department);
          try
          {
            _entities.ContainsKey(t.ID);
            _entities.Add(t.ID, t);
            _teamsCount++;
            return true;
          }
          catch (MyOrgHashTable.KeyAlreadyExists)
          {
            return false;
          }
        }
      }
      return false;
    }
    public bool AddEmployee(string lastName, string firstName, string middleName, DateTime dob, Sex sex, SID office, JobTitle jobTitle)
    {
      if (_employeesCount < _employeesMax)
      {
        if (Exists(office))
        {
          Employee t = new Employee(firstName, middleName, lastName, dob, sex, Fetch(office) as Unit, jobTitle);
          try
          {
            _entities.ContainsKey(t.ID);
            _entities.Add(t.ID, t);
            _employeesCount++;
            return true;
          }
          catch (MyOrgHashTable.KeyAlreadyExists)
          {
            return false;
          }
        }
      }
      return false;
    }
    bool Exists(SID id)
    {
      return _entities.ContainsKey(id);
    }
    IRanked Fetch(SID id)
    {
      try
      {
        return _entities[id];
      }
      catch (Exception ex) when (ex is MyOrgHashTable.HashTableEmpty || ex is MyOrgHashTable.NoSuchKey)
      {
        return null;
      }
    }
    List<SID> FindAll(Predicate<IRanked> predicate)
    {
      return _entities.FindAll(predicate);
    }
    List<IRanked> FindAllVal(Predicate<IRanked> predicate)
    {
      return _entities.FindAllVal(predicate);
    }

    public IQuery GetQuery()
    {
      return new Query(this);
    }
    public IQuery GetCities()
    {
      return new Query(this);
    }
    public IQuery GetBranches()
    {
      return new Query(this, Query.Column.Branch);
    }
    public IQuery GetDepartments()
    {
      return new Query(this, Query.Column.Department);
    }
    public IQuery GetTeams()
    {
      return new Query(this, Query.Column.Team);
    }
    public IQuery GetEmployees()
    {
      return new Query(this, Query.Column.Employee);
    }
  }
  public class QueryResolver
  {
    public class BadQuery : Exception { }
    public class BadExpression : Exception { }

    Organization _org;
    IQuery _query;

    static Query.Column GetColumn(string subexpr)
    {
      if (Regex.IsMatch(subexpr, @"^[Cc]ity$"))
        return Query.Column.City;
      else if (Regex.IsMatch(subexpr, @"^[Bb]ranch$"))
        return Query.Column.Branch;
      else if (Regex.IsMatch(subexpr, @"^[Dd]epartment$"))
        return Query.Column.Department;
      else if (Regex.IsMatch(subexpr, @"^[Tt]eam$"))
        return Query.Column.Team;
      else if (Regex.IsMatch(subexpr, @"^[Ee]mployee$"))
        return Query.Column.Employee;
      else
        throw new BadExpression();
    }
    static SID GetSID(string subexpr)
    {
      return new SID(subexpr);
    }

    QueryResolver() { }
    public QueryResolver(Organization organization)
    {
      _org = organization;
    }
    public QueryResolver Copy()
    {
      return new QueryResolver() { _org = this._org, _query = this._query.Copy() };
    }
    public List<string> Resolve(string expr)
    {
      List<string> r = new List<string> { };

      Regex query = new Regex(@"^\s*(?<Query>chain|fork(?:\s+new)?|new\s+query|lookup(?:\s+pick)?)?(?<expr>.*)$");
      Regex select = new Regex(@"^\s*\[(?<Column>\w+)\](?<expr>.*)$");
      Regex wherecol = new Regex(@"^\s*\(\s*\[(?<Column>\w+)\]\s*=\s*\{(?<Value>[\p{IsCyrillic}\w\-\ \""]+)\}\s*\)(?<expr>.*)$");
      Regex whereid = new Regex(@"^\s*\(\s*\{(?<ID>[\p{IsCyrillic}\w\-\ \""\>\|\.]+)\}\s*\)(?<expr>.*)$");
      Regex tolookup = new Regex(@"^\s*\{(?<ID>[\p{IsCyrillic}\w\-\ \""\>\|\.]+)\}\s*$");
      Regex finalizer = new Regex(@"^(?<expr>.*)(?<Finalizer>(?:\?(?:\[\d+\])?(?:(?<=\[\d+\])\s+(?:lookup|pick)|(?:\:\w+))?|\#)?)\s*$", RegexOptions.RightToLeft);

      IQuery q, s = _query;
      Match sub;

      Match hm, tm;
      string head, tail;
      hm = query.Match(expr);
      head = hm.Groups["Query"].Value;
      expr = hm.Groups["expr"].Value;
      tm = finalizer.Match(expr);
      tail = tm.Groups["Finalizer"].Value;
      expr = tm.Groups["expr"].Value;
      if (!hm.Success || !tm.Success)
      {
        throw new BadQuery();
      }
      else if ((sub = Regex.Match(head, @"^chain$|^$|^(?<NewQuery>new\s+query)$")).Success)
      {
        if (!sub.Groups["NewQuery"].Success && _query != null)
          q = s = _query.Copy();
        else if ((sub = select.Match(expr)).Success)
        {
          expr = sub.Groups["expr"].Value;
          switch (GetColumn(sub.Groups["Column"].Value))
          {
            case Query.Column.City:
              s = _org.GetCities();
              break;
            case Query.Column.Branch:
              s = _org.GetBranches();
              break;
            case Query.Column.Department:
              s = _org.GetDepartments();
              break;
            case Query.Column.Team:
              s = _org.GetTeams();
              break;
            case Query.Column.Employee:
              s = _org.GetEmployees();
              break;
            default:
              throw new BadQuery();
          }
          q = s;
        }
        else
          q = s = _org.GetQuery();
      }
      else if ((sub = Regex.Match(head, @"^fork(?:\s+(?<ForkNew>new))?$")).Success && !Regex.IsMatch(tail, @"^\?\[\d+\]\s+pick$"))
      {
        if (!sub.Groups["ForkNew"].Success && _query != null)
          q = (s = _query).Copy();
        else if ((sub = select.Match(expr)).Success)
        {
          expr = sub.Groups["expr"].Value;
          switch (GetColumn(sub.Groups["Column"].Value))
          {
            case Query.Column.City:
              q = _org.GetCities();
              break;
            case Query.Column.Branch:
              q = _org.GetBranches();
              break;
            case Query.Column.Department:
              q = _org.GetDepartments();
              break;
            case Query.Column.Team:
              q = _org.GetTeams();
              break;
            case Query.Column.Employee:
              q = _org.GetEmployees();
              break;
            default:
              throw new BadQuery();
          }
        }
        else
          q = _org.GetQuery();
      }
      else if ((sub = Regex.Match(head, @"^lookup(?:\s+(?<LookupPick>pick))?$")).Success && !Regex.IsMatch(tail, @"^\?\[\d+\].*$"))
      {
        Match lm;
        if ((lm = tolookup.Match(expr)).Success)
        {
          expr = "";
          if (!sub.Groups["LookupPick"].Success)
          {
            try
            {
              q = Query.Lookup(_org, GetSID(lm.Groups["ID"].Value));
            }
            catch (Query.NoResult)
            {
              throw new BadQuery();
            }
          }
          else
          {
            try
            {
              q = s = Query.Lookup(_org, GetSID(lm.Groups["ID"].Value));
            }
            catch (Query.NoResult)
            {
              throw new BadQuery();
            }
          }
        }
        else
          throw new BadQuery();
      }
      else
      {
        throw new BadQuery();
      }
      while (true)
      {
        if ((sub = select.Match(expr)).Success)
        {
          q.Select(GetColumn(sub.Groups["Column"].Value));
          expr = sub.Groups["expr"].Value;
        }
        else if ((sub = whereid.Match(expr)).Success)
        {
          q = q.Where(GetSID(sub.Groups["ID"].Value));
          expr = sub.Groups["expr"].Value;
        }
        else if ((sub = wherecol.Match(expr)).Success)
        {
          q = q.Where(GetColumn(sub.Groups["Column"].Value), sub.Groups["Value"].Value);
          expr = sub.Groups["expr"].Value;
        }
        else if (Regex.IsMatch(expr, @"^\s*$"))
        {
          break;
        }
        else
        {
          throw new BadQuery();
        }
      }
      if ((sub = Regex.Match(tail, @"^\?(\[(?<Index>\d+)\])?(?:(?:(?<=\1)\s+(?<Action>lookup|pick))|(?:\:(?<Field>\w+))?)$")).Success)
      {
        if (sub.Groups["Index"].Success)
        {
          int i;
          try
          {
            i = Convert.ToInt32(sub.Groups["Index"].Value);
          }
          catch (FormatException)
          {
            throw new BadQuery();
          }
          if (i >= q.ResultsCount())
          {
            throw new BadQuery();
          }
          if (Regex.IsMatch(sub.Groups["Action"].Value, @"^lookup$"))
            r = Resolve($"lookup {{{q.ResultFields(i)["ID"]}}} ?");
          else if (Regex.IsMatch(sub.Groups["Action"].Value, @"^pick$"))
            q = s = Query.Lookup(_org, q.ResultFields(i)["ID"] as SID);
          else if (sub.Groups["Field"].Success)
          {
            try
            {
              r.Add($"{q.ResultFields(i)[sub.Groups["Field"].Value]}");
            }
            catch (KeyNotFoundException)
            {
              throw new BadQuery();
            }
          }
          else
          {
            if ((Query.Column)q.ResultFields(i)["Type"] == Query.Column.Employee)
              r.Add($"    {q.ResultFields(i)["Name"],-43}{$"{{{q.ResultFields(i)["ID"]}}}",53}");
            else
              r.Add($"    {q.ResultFields(i)["Name"],-26}{$"{{{q.ResultFields(i)["ID"]}}}",70}");
          }
        }
        else if (q.ResultsCount() == 1)
        {
          if (sub.Groups["Field"].Success)
          {
            try
            {
              r.Add($"{q.ResultFields(0)[sub.Groups["Field"].Value]}");
            }
            catch (KeyNotFoundException)
            {
              throw new BadQuery();
            }
          }
          else
          {
            IQuery w;
            switch ((Query.Column)q.ResultFields(0)["Type"])
            {
              case Query.Column.City:
                r.Add($"► {q.ResultFields(0)["Name"],-28}{$"{{{q.ResultFields(0)["ID"]}}}",70}");
                w = q.Copy().Select(Query.Column.Branch);
                for (int i = 0; i < w.ResultsCount(); i++)
                  r.Add($"  {(i != w.ResultsCount() - 1 ? "├" : "└")}─{w.ResultFields(i)["Name"],-26}{$"{{{w.ResultFields(i)["ID"]}}}",70}");
                break;
              case Query.Column.Branch:
                w = Query.Lookup(_org, (q.ResultFields(0)["City"] as IRanked).ID);
                r.Add($"  {w.ResultFields(0)["Name"],-28}{$"{{{w.ResultFields(0)["ID"]}}}",70}");
                r.Add($"► └─{q.ResultFields(0)["Name"],-26}{$"{{{q.ResultFields(0)["ID"]}}}",70}");
                w = q.Copy().Select(Query.Column.Employee).Where(JobTitle.Branch.BranchDirector);
                r.Add($"  ♠ ├─{w.ResultFields(0)["Name"],-42}{$"{{{w.ResultFields(0)["ID"]}}}",52}");
                w = q.Copy().Select(Query.Column.Department);
                for (int i = 0; i < w.ResultsCount(); i++)
                  r.Add($"    {(i != w.ResultsCount() - 1 ? "├" : "└")}─{w.ResultFields(i)["Name"],-24}{$"{{{w.ResultFields(i)["ID"]}}}",70}");
                break;
              case Query.Column.Department:
                w = Query.Lookup(_org, (q.ResultFields(0)["City"] as IRanked).ID);
                r.Add($"  {w.ResultFields(0)["Name"],-28}{$"{{{w.ResultFields(0)["ID"]}}}",70}");
                w = Query.Lookup(_org, (q.ResultFields(0)["Branch"] as IRanked).ID);
                r.Add($"  └─{w.ResultFields(0)["Name"],-26}{$"{{{w.ResultFields(0)["ID"]}}}",70}");
                r.Add($"  ► └─{q.ResultFields(0)["Name"],-24}{$"{{{q.ResultFields(0)["ID"]}}}",70}");
                w = q.Copy().Select(Query.Column.Employee).Where(JobTitle.Department.DepartmentHead);
                r.Add($"    ♠ ├─{w.ResultFields(0)["Name"],-41}{$"{{{w.ResultFields(0)["ID"]}}}",51}");
                w = q.Copy().Select(Query.Column.Team);
                for (int i = 0; i < w.ResultsCount(); i++)
                  r.Add($"      {(i != w.ResultsCount() - 1 ? "├" : "└")}─{w.ResultFields(i)["Name"],-22}{$"{{{w.ResultFields(i)["ID"]}}}",70}");
                break;
              case Query.Column.Team:
                w = Query.Lookup(_org, (q.ResultFields(0)["City"] as IRanked).ID);
                r.Add($"  {w.ResultFields(0)["Name"],-28}{$"{{{w.ResultFields(0)["ID"]}}}",70}");
                w = Query.Lookup(_org, (q.ResultFields(0)["Branch"] as IRanked).ID);
                r.Add($"  └─{w.ResultFields(0)["Name"],-26}{$"{{{w.ResultFields(0)["ID"]}}}",70}");
                w = Query.Lookup(_org, (q.ResultFields(0)["Department"] as IRanked).ID);
                r.Add($"    └─{w.ResultFields(0)["Name"],-24}{$"{{{w.ResultFields(0)["ID"]}}}",70}");
                r.Add($"    ► └─{q.ResultFields(0)["Name"],-22}{$"{{{q.ResultFields(0)["ID"]}}}",70}");
                w = q.Copy().Select(Query.Column.Employee).Where(JobTitle.Team.TeamLead);
                r.Add($"      ♠ ├─{w.ResultFields(0)["Name"],-40}{$"{{{w.ResultFields(0)["ID"]}}}",50}");
                w = q.Copy().Select(Query.Column.Employee).Where(JobTitle.Team.TeamMember);
                for (int i = 0; i < w.ResultsCount(); i++)
                  r.Add($"        {(i != w.ResultsCount() - 1 ? "├" : "└")}─{w.ResultFields(i)["Name"],-40}{$"{{{w.ResultFields(i)["ID"]}}}",50}");
                break;
              case Query.Column.Employee:
                r.Add($"{"".PadLeft(90, '═')}\n" +
                      $"{((string)q.ResultFields(0)["Name"]).PadLeft(45 + ((string)q.ResultFields(0)["Name"]).Length / 2)}\n" +
                      $"{"".PadLeft(90, '═')}\n" +
                      $"{"ID",-19}:{$"{{{q.ResultFields(0)["ID"]}}}",70}\n" +
                      $"{"DoB",-19}:{(DateTime)q.ResultFields(0)["DoB"],70:dd.MM.yyyy}\n" +
                      $"{"Sex",-19}:{(Sex)q.ResultFields(0)["Sex"],70}\n" +
                      $"{"Office",-19}:{$"{{{q.ResultFields(0)["Office"]}}}",70}\n" +
                      $"{"Job Title",-19}:{q.ResultFields(0)["JobTitle"],70}");
                break;
            }
          }
        }
        else
        {
          if (sub.Groups["Field"].Success)
          {
            try
            {
              for (int i = 0; i < q.ResultsCount(); i++)
                r.Add($"{q.ResultFields(i)[sub.Groups["Field"].Value]}");
            }
            catch (KeyNotFoundException)
            {
              throw new BadQuery();
            }
          }
          else
          {
            for (int i = 0; i < q.ResultsCount(); i++)
            {
              if ((Query.Column)q.ResultFields(i)["Type"] == Query.Column.Employee)
                r.Add($"    {q.ResultFields(i)["Name"],-43}{$"{{{q.ResultFields(i)["ID"]}}}",53}");
              else
                r.Add($"    {q.ResultFields(i)["Name"],-26}{$"{{{q.ResultFields(i)["ID"]}}}",70}");
            }
          }
        }
      }
      else if (Regex.IsMatch(tail, @"^\#$"))
      {
        r.Add($"{q.ResultsCount()}");
      }
      _query = s;
      return r;
    }
  }
}
